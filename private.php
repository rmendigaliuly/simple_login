<?php
	session_start();
	if (!$_SESSION["user_id"]) header("Location: login.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Private Zone</title>
</head>
<body>
	Добро пожаловать на приватную зону.
	<br><p><a href="index.php">Назад</a></p>
	<br><p><a href="login.php?logout=1">Выйти</a>
</body>
</html>