<?php

session_start();


function set_error () {
	$_SESSION["error_msg"] = "Имя пользователя или пароль недействительный.";
	header("Location: login");
}

function login ($username) {
	if (isset($GLOBALS["db"]))
	{
		$db = $GLOBALS["db"];
	}
	else
	{
		$db = mysqli_connect("localhost", "root", "", "simple_login");
	}

	$query = "select id from users where username = '" . $username . "'";
	$db_result = mysqli_query($db, $query);
	$user_id = mysqli_fetch_row($db_result)[0];
	
	$_SESSION["user_id"] = $user_id;
	header("Location: private.php");
}

if (isset($_SESSION["error_msg"]))
{
	$err_msg = $_SESSION["error_msg"];
	unset($_SESSION["error_msg"]);
}

if (isset($_POST["submit"]))
{
	$db = mysqli_connect("localhost", "root", "", "simple_login");
	if (mysqli_connect_errno()) {
		die('Database connection failed: ' . mysqli_connect_error() . ' (' . mysqli_connect_errno() . ')');
	}

	$typed_name = $_POST["username"];
	$typed_password = $_POST["password"];
	
	$query = "select username from users";
	$db_result = mysqli_query($db, $query);
	if (!$db_result) {
		die('Database query failed'); 
	}

	$usernames = [];
	while ($row = mysqli_fetch_assoc($db_result)) {
		$usernames[] = $row["username"];
	}
	mysqli_free_result($db_result);

	if (!in_array($typed_name, $usernames))
	{
		set_error ();
	}
	else
	{
		$query = "select password from users where username = '".$typed_name."'";
		$db_result = mysqli_query($db, $query);
		$hash = mysqli_fetch_row($db_result)[0];
		mysqli_free_result($db_result);

		if (!password_verify($typed_password, $hash))
		{
			set_error ();
		}
		else login ($typed_name);
	}
	mysqli_close($db);
}

if (isset($_GET["logout"]) && $_GET["logout"])
{
	unset($_SESSION["user_id"]);
	header("Location: " . $_SERVER['HTTP_REFERER']);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
</head>
<body>
	<?php 
		if (isset($err_msg)) 
		{
			echo  $err_msg . "<br>";
		}
	?>
	<form action = "<?php echo $_SERVER['PHP_SELF']?>" method="POST">
		<input name="username" type="text" placeholder="Ваше имя"><br>
		<input name="password" type="password" placeholder="Пароль"><br>
		<input type="submit" value="Войти" name="submit">
	</form>
</body>
</html>