<?php
	session_start();
	if (isset($_SESSION["user_id"])) 
	{
		$link_text = "Выйти";
		$link = "login.php?logout=1";
	}
	else 
	{
		$link_text ="Войти";
		$link = "login.php";
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
</head>
<body>
	<p><a href="<?php echo $link;?>"><?php echo $link_text;?></a></p>
</body>
</html>